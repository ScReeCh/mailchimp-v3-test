<?php

class Helpers {

	public static function decodeRequest(&$request) {
		$request = json_decode($request,true);
	}

	public static function hasData($object) {
		
		if(is_array($object)) {
		
			return count($object) > 0 ? true : false;
		
		} elseif(is_object($object)) {

			$count = 0;
			foreach ($object as $key => $value) {
				return true;
				break;
			}

			return false;
		}
	}

	public static function toArray($object) {
		
		$array = [];

		foreach ($object as $key => $value) {
			
			if(is_object($value)) {
				$array[$key] = (array)self::toArray($value);
			} elseif(is_array($value)) {
				$array[$key] = self::toArray($value);
			} else {
				$array[$key] = $value;
			}
		}

		return $array;
	}
}