<?php

class MailChimp {

	const API_KEY = 'YOUR API KEY HERE';
	const URL     = 'https://us9.api.mailchimp.com/3.0';
	
	/**
	 * Reguest Obejct
	 * @var object
	 */
	private $request;

	public function __construct() {
		$this->setRequest();

		$this->getRequest()->setApi(self::API_KEY);
		$this->getRequest()->setUrl(self::URL);
	}
	
	/**
	 * Request to view lists
	 * @return array | Collection of lists
	 */
	public function viewLists()
	{
		$this->getRequest()->setRequestType('GET');
		$this->getRequest()->setRoute('/lists');
		
		return $this->getRequest()->sendRequest()['lists'];
	}

	/**
	 * Request create new list
	 * @param  string | $request | JSON string
	 * @return array | Create list callback
	 */
	public function createNewList($request)
	{	
		Helpers::decodeRequest($request);
		
		$this->getRequest()->setRequestType('POST');

		$this->getRequest()->setRoute('/lists');
		
		$this->getRequest()->setQuery($request);
		
		return  $this->getRequest()->sendRequest();
	}

	/**
	 * Add new member to list
	 * @param int | $listId | List id
	 * @param  string | $request | JSON string
	 * @return array | New member callback
	 */
	public function addMembersToList($listId,$request)
	{	
		Helpers::decodeRequest($request);

		$this->getRequest()->setRequestType('POST');
		$this->getRequest()->setRoute('/lists/'.$listId.'/members');
		$this->getRequest()->setQuery($request);
		
		return $this->getRequest()->sendRequest();
	}

	/**
	 * Update member in list
	 * @param  int | $listId | List id
	 * @param  string | $memberId | List member id
	 * @param  string | $request | JSON string
	 * @return array | Update member callback
	 */
	public function updateMembersInList($listId,$memberId,$request)  
	{
		Helpers::decodeRequest($request);

		$this->getRequest()->setRequestType('PUT');
		$this->getRequest()->setRoute('/lists/'.$listId.'/members/'.$memberId);
		$this->getRequest()->setQuery($request);
		
		return $this->getRequest()->sendRequest();
	}

	/**
	 * Set request object
	 */
	private function setRequest() {
		$this->request = new Requests;
	}

	/**
	 * Get request object
	 */
	private function getRequest() {
		return $this->request;
	}
}