<?php

class Requests extends GuzzleHttp\Client  {

	private $requestHeaders;
	private $route;
	private $url;
	private $query;
	private $requestType;

	/**
	 * Set API key
	 * @param string | $key | Request API key
	 */
	public function setApi($key = null) {
		
		$this->requestHeaders = ['headers' => ['Authorization' => 'apikey '.$key]];
		parent::__construct($this->requestHeaders);
	}

	/**
	 * Set request route
	 * @param string | $route | Route used to access data. The query after URL.
	 */
	public function setRoute($route = null) {
		$this->route = $route;
	}

	/**
	 * Set request URL
	 * @param string | $url | Request URL
	 */
	public function setUrl($url) {
		$this->url = $url;		
	}

	/**
	 * Set the POST,PUT,PATCH request
	 * @param string | $query | Requst data
	 */
	public function setQuery($query) {

		if(is_array($query)) {
			$this->query = $query;
		} else {
			Errors::setError('Query is not an Array, '.gettype($query).' given!',Errors::VALIDATION_ERROR);
		}
		
	}

	/**
	 * Requst type ex. GET, POST, PUT, PATCH
	 * @param string | $requestType | Type of request
	 */
	public function setRequestType($requestType) {
		$this->requestType = strtoupper($requestType);
	}

	/**
	 * Send requst
	 * @return array | Callback
	 */
	public function sendRequest() {

		if($this->route == null) {
			Errors::setError('ROUTE, not set!',Errors::VALIDATION_ERROR);
		}

		if($this->url == null) {
			Errors::setError('URL, not set!',Errors::VALIDATION_ERROR);
		}

		$request = array('verify' => false);

		if(($this->requestType == 'POST' || $this->requestType == 'PUT' || $this->requestType == 'PATCH') && helpers::hasData($this->query)) {
			if(!isset($this->query)) {
				Errors::setError('QUERY TYPE, not set!',Errors::VALIDATION_ERROR);
			}

			$request['json'] = $this->query;

		}
	
		try {
			$response = $this->request($this->requestType, $this->url.$this->route,$request);
		} catch (ClientException $e) {
		    Errors::setError(json_decode($e->getResponse()->getBody(), true),Errors::VALIDATION_ERROR);		    
		}	

		if($response != null) {
			return json_decode($response->getBody(), true);
		}
		
	}

}