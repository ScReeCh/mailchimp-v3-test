<?php

class Response {

	const RESPONSE_SUCCESS = 'success';
	const RESPONSE_ERROR = 'error';

	private $data;
	
	/**
	 * Set default response data
	 */
	public function __construct() {
    	
    	$this->response = self::RESPONSE_SUCCESS;
    	$this->data = [];
   	}

   	/**
   	 * Initialize response object
   	 */
   	public static function initialize() {
   		$response = new Response;
   		return $response;
   	}

   	/**
   	 *
   	 * Set response data
   	 * 
   	 * @param array $data Response data
   	 */
	public function setResponseData($data)
	{	
		$thisData = $data;
		$this->data = $thisData;

		return $this;
	}

   	/**
   	 *
   	 * Add response data
   	 * 
   	 * @param array $data Response data
   	 */
	public function addResponseData($data)
	{	
		$thisData = $this->data;
		
		$responseData = new stdClass;
		
		if(Helpers::hasData($data)) {
			foreach ($data as $dataKey => $dataValue) {
				$responseData->$dataKey = $dataValue;
			}
		}
		
		$thisData[] = $responseData;

		$this->data = $thisData;

		return $this;
	}

	/**
	 *
	 * Set error response
	 * 
	 * @param array $errors Error responses
	 */
	public function setErrors()
	{	
		if( Helpers::hasData(Errors::getErrors()) ) {

			$this->response = self::RESPONSE_ERROR;

			// Reset data
			$this->data = [];

			$thisData = $this->data;
			
			$errorData = new stdClass;
			
			if(Helpers::hasData(Errors::getErrors())) {
				$errors = Errors::getErrors();
				
				foreach ($errors as $dataKey => $dataValue) {
					$thisData[] = $dataValue;
				}
			}
			
			$this->data = $thisData;
			$this->error = $errors = Errors::getErrorTypes();
			return $this;
		}
	}

	/**
	 *
	 * Get response object
	 * 
	 * @return [type]
	 */
	public function getRespose()
	{	
		$this->setErrors();

		return $this;
	}

	/**
	 * 
	 * Format into json response
	 * 
	 * @return [type]
	 */
	public function json()
	{	
		return json_encode($this->data);
	}
}