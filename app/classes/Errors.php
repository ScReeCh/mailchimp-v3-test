<?php

class Errors {
	
	const SYSTEM_ERROR     = 1;
	const VALIDATION_ERROR = 2;

	private static $errorCollection;
	private static $errorType;

	/**
	 * Set error
	 * @param string | $errorMessage | Error message
	 * @param int | $errorType | Error type code
	 */
	public static function setError($errorMessage,$errorType) {
		
		if($errorType == 1) {
			
		} elseif($errorType == 2) {

			self::$errorCollection = [];

			if(is_array($errorMessage)) {
				foreach ($errorMessage as $key => $value) {
					self::$errorCollection[] = $value;
				}
			} elseif(is_string($errorMessage)) {
				self::$errorCollection[] = $errorMessage;
			} else {
				// Throw system errror
			}
			
			self::$errorType = $errorType;
		}
		
	}

	/**
	 * Get all cought errors
	 * @return array | All errors
	 */
	public static function getErrors() {
		return self::$errorCollection;
	}

	/**
	 * Check to see it there are any error
	 * @return boolean | Are there any errors
	 */
	public static function hasErrors() {
		
		if(Helpers::hasData(self::$errorCollection)) {
			return true;
		} else {
			return false;
		}
		
	}

	/**
	 * Reset errors
	 */
	public static function reset() {
		self::$errorCollection = null;
	}
}