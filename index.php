<?php

	function dd($output)
	{
		echo "<pre>";
		var_dump($output);
		exit();
	}

	function vdd($output)
	{
		echo "<pre>";
		var_dump($output);
	}

require __DIR__ . '/vendor/autoload.php';

$view = $_GET['v'];

$response = Response::initialize();

$mailChimp = new MailChimp;


$createNewList        = "{}";   // Insert JSON string here to create a list.
$listId               = 0;      // Insert JSON string here to create a list.
$memberDetails        = "{}";   // Insert JSON string here to create a list.
$memberId             = 0;      // Subscriber Id
$memberUpdatedDetails = "{}";   // Insert JSON string here to create a list.

switch ($view) {
    case 'view-lists':
        $response->setResponseData($mailChimp->viewLists());
        break;
    case 'create-lists':
        $response->setResponseData($mailChimp->createNewList($createNewList)); 
        break;
    case 'add-member-to-list':
        $response->setResponseData($mailChimp->addMembersToList($listId,$memberDetails));
        break;
    case 'update-member-to-list':
        $response->setResponseData($mailChimp->updateMembersInList($listId,$memberId,$memberDetails));
        break;
    default:
    	echo '<div><a href="/?v=view-lists">View lists</a></div>';
    	exit();
    break;
}

echo $response->getRespose()->json();